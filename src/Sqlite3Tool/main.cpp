//
//  SQLiteW.mm
//
//
//  Created by Applax on 2015/11/12.
//
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "SQLiteW.h"

enum FileError {
	StatError = 10,
	OpenError,
	ReadError,
};

void XOREncrypt(unsigned int enckey, unsigned char* data, unsigned int length) {
	unsigned int* ptr = (unsigned int*)data;
	unsigned int max = length / sizeof(unsigned int);
	printf("encrypt:%u", enckey);
	for (unsigned int i = 0; i < max; i++) {
		ptr[i] ^= enckey;
	}
	for (unsigned int i = 0; i < 16; i++) {
		ptr[i] ^= ptr[max-16+i];
	}
}

int main(int argc, char* argv[]) {
	if(argc < 6) {
		printf("arguments:\n"
			"1:mode 2:dbName 3:dbKey 4:group 5:path 6:key");
	}

	int ret = 0;
	const char mode = argv[1][0];
	const char* dbName = argv[2];
	const char* enckey = argv[3];
	SQLiteW::GROUP group = argv[4][0] - '0';
	const char* path = argv[5];
	const char* key = argv[6];

	struct stat st;
	FILE* fp;
	unsigned char* buff;

	try {
		// DBインスタンス生成
		SQLiteW db(dbName);
		// テーブルが無ければ作成
		db.createKeyValueTable();

		switch(mode) {
			case 'u':	// 新規 or 更新
				if (stat(path, &st) != 0)
				{
					printf("StatError:path[%s]\n", path);
					return StatError;
				}
				fp = fopen(path, "rb");
				if (fp == nullptr)
				{
					printf("FileOpenError:path[%s]\n", path);
					return OpenError;
				}
				buff = new unsigned char[st.st_size];
				if (fread(buff, 1, st.st_size, fp) < st.st_size)
				{
					printf("FileReadError:path[%s]\n", path);
					return 	ReadError;
				}
				if (key[0] == '!')
				{
					XOREncrypt(atoi(enckey), buff, st.st_size);
				}
				db.setValue(key, buff, st.st_size, group);
				printf("Updated:group[%d], key[%s]\n", group, key);
				break;

			case 'd':	// 削除
				if(!db.deleteKey(key, group))
				{
					return SQLiteW::ExecErr;
				}
				printf("Deleted:group[%d], key[%s]\n", group, key);
				break;
		}

	} catch (FileError e) {
		ret = e;
		printf("FileError:[%d]\n", e);
	} catch (SQLiteW::Error e) {
		ret = e;
		printf("DBError[%d]\n", e);
	}

	delete[] buff;
	fclose(fp);

	return ret;
}
