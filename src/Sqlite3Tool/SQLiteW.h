//
//  SQLiteW.h
//
//  Created by Applax on 2015/11/12.
//
//

#import "sqlite3.h"
#import <string>

#define SQLITE_KVT_NAME "t_kv"

using std::string;

class SQLiteW {
public:
	typedef unsigned short GROUP;
	enum Error {
		OpenErr = 1,	// オープンエラー
		ExecErr			// 実行時エラー
	};
	
	struct BLOB {
		unsigned char* buff;
		unsigned int length;

		BLOB() : buff(0), length(0) {}
		~BLOB() {
			if (buff != 0) {
				delete[] buff;
				buff = 0;
			}
		}
	};
	
	SQLiteW(const char* dbname, const char* pass = 0);
	virtual ~SQLiteW();
	
	// Key-Valueテーブル作成（アプリ起動時に1回呼び出す）
	// 初回作成時はtrueを返す
	bool createKeyValueTable();
	
	// 暗号化（暗号化を解除した場合に使用）
	void encrypt();
	// 暗号化キー変更
	void changeEncryptKey(const char* newPass);
	// 暗号化解除
	void decrypt();

	
	// レコード削除
	bool deleteKey(const char* key, GROUP group = 0);

	
	// 取得
	// BLOB（引数のBLOBが保持するポインタ先にバッファを確保する）
	void getValue_Blob(const char* key, BLOB& blob, GROUP group = 0);
	// REAL
	double getValue_Real(const char* key, GROUP group = 0) {
		return getValue<double>(key, sqlite3_column_double, group);
	};
	// INT
	int getValue_Int(const char* key, GROUP group = 0) {
		return getValue<int>(key, sqlite3_column_int, group);
	};
	// TEXT
	string getValue_Text(const char* key, GROUP group = 0);
	

	// 設定
	// MEMO:内部でトランザクションが発行されているため、連続のsetValueは非常に遅い。
	//      連続でSQLを発行する場合は、begin-commit/rollbackを開始、終了時に発行すること。
	// BLOB
	void setValue(const char* key, const BLOB& blob, GROUP group = 0) {
		setValue(key, blob.buff, blob.length, group);
	}
	void setValue(const char* key, const unsigned char* value, unsigned int length, GROUP group = 0);
	// REAL
	void setValue(const char* key, double value, GROUP group = 0) {
		setValue<double, TYPE_REAL>(key, value, group);
	}
	// INT
	void setValue(const char* key, int value, GROUP group = 0) {
		setValue<int, TYPE_INT>(key, value, group);
	}
	// TEXT
	void setValue(const char* key, const string value, GROUP group = 0);
	
	
	
	
	// トランザクションの開始
	void begin();
	// コミット
	void commit();
	// ロールバック
	void rollbak();
/*
	virtual void openRDB();
	virtual void query(const char* sql);
	virtual const std::string* getResult() const { return m_Result; }
	
	virtual void exec(const char* sql);
 */
	
private:
	enum TYPE {
		TYPE_REAL,
		TYPE_INT,
	};
	static const char* KVT_SELECT;
	static const char* GET_VALUE_SQL;
	static const char* SET_VALUE_SQL;

	
//	sqlite3* m_RDB;
	sqlite3* m_KeyValueDB;
	const char* m_Pass;
	unsigned int cmdCountOnTransaction;
	
	
	// GroupとKeyに関連づくValueを取得（BLOB以外、TEXT以外）
	template<typename T, typename FT>
	T getValue(const char* key, FT func, GROUP group = 0) {
//		assert(m_KeyValueDB);

		sqlite3_stmt *stmt;
		T value;

		int ret = sqlite3_prepare_v2(m_KeyValueDB, GET_VALUE_SQL, -1, &stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_int(stmt, 1, group);
			sqlite3_bind_text(stmt, 2, key, -1, SQLITE_TRANSIENT);
			ret = sqlite3_step(stmt);
			if (ret == SQLITE_ROW) {
				value = (T)func(stmt, 0);
//				NSLog(@"Gat Data. Group:[%d] Key:[%s] Value:[SCALAR]", group, key);
			} else if (ret == SQLITE_DONE) {
				value = 0;
//				NSLog(@"No Data.(%d) Group:[%d] Key:[%s]", ret, group, key);
			}
		}
		
		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);
		if (ret != SQLITE_OK && ret != SQLITE_ROW && ret != SQLITE_DONE) {
			throw ExecErr;
		}
		
		return value;
	}
	// GroupとKeyに関連づくValueを更新（BLOB、TEXT以外）
	template<typename T, TYPE FT>
	void setValue(const char *key, T value, GROUP group) {
//		assert(m_KeyValueDB);
		
		sqlite3_stmt *stmt;
		
		int ret = sqlite3_prepare_v2(m_KeyValueDB, SET_VALUE_SQL, -1, &stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_int(stmt, 1, group);
			sqlite3_bind_text(stmt, 2, key, -1, SQLITE_TRANSIENT);
			switch (FT) {
				case TYPE_REAL: sqlite3_bind_double(stmt, 3, value); break;
				case TYPE_INT: sqlite3_bind_int64(stmt, 3, value); break;
			}
			ret = sqlite3_step(stmt);
			if (ret == SQLITE_DONE) {
				// トランザクションが有効な場合コマンド実行数をカウント
				if (cmdCountOnTransaction > 0) {
					cmdCountOnTransaction++;
				}
	//			switch (FT) {
	//				case TYPE_REAL: NSLog(@"Set Data. Group:[%d] Key:[%s] Value:[%f]", group, key, (double)value); break;
	//				case TYPE_INT:  NSLog(@"Set Data. Group:[%d] Key:[%s] Value:[%d]", group, key, (int)value); break;
	//			}
			} else {
				
	//			switch (FT) {
	//				case TYPE_REAL: NSLog(@"Set Failure.(%d) Group:[%d] Key:[%s] Value:[%f]", ret, group, key, (double)value); break;
	//				case TYPE_INT:  NSLog(@"Set Failure.(%d) Group:[%d] Key:[%s] Value:[%d]", ret, group, key, (int)value); break;
	//			}
			}
		}
		
		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);
		if(ret != SQLITE_DONE) {
			throw ExecErr;
		}
	}
//	static int callback(void *_, int argc, char **argv, char **columnName);
};
