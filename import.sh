#!/bin/bash -u
. option.sh

# $1:folder $2:group 
function updateResource() {
	local outlist="$OUTFOLDER/group$2.txt"
	cat $LISTFILE | while read file
	do
		local enc=${file:0:1}
		local key=''
		if [ $enc = '!' ]; then
			key=${file:1}
		else
			key=''$file
		fi
		echo $file >> $outlist
	    ./Sqlite3Tool u $outfile $ENCKEY $2 $1/$key $file
	done
}

outfile=$OUTFOLDER/$DBNAME
mkdir -p $OUTFOLDER
rm -rf $outfile
updateResource $INFOLDER 0
#updateResource './dat' 1
mv $outfile $MOVEFOLDER/$DBNAME
